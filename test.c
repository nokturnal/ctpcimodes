/**
    Copyright 2010-2011 Pawel Goralski
    See license.txt for licensing information.
*/

//C standard libraries
#include <math.h>
#define M_PI	3.1415926535897932384626433832795f

//png loading functionality
#include "inc/png_load.h"

//radeon helper functions
#include "inc/rad_help.h"

//radeon inline hardware acccelerated graphic functions
#include "inc/rad_gfx.h"

//debug/logging function 
#include "inc/debug.h"

//defines and structures for dealing with CTPCI api
#include "inc/ctpci_defs.h"

/* ikbd stuff for keyboard input */
#include "inc/ikbd.h"
#include "inc/scancode.h"

//texture names
#define BG_IMG "test.png"
#define TX_IMG "tx.png"
#define LOGO_IMG "burning.png"

//target video mode we are setting 640x480x32BPP@75mhz
// other example screen modes:
// 320x200x8BPP@70hz   0x4263
// 320x200x16BPP@70hz  0x4264
// 320x200x32BPP@70hz  0x4265 pixelformat: 0x00RRGGBB
// 640x480x16BPP@75hz  0x4144 
// 640x480x32BPP@75hz  0x4145 pixelformat: 0x00RRGGBB

#define VMODE 0x4145 

#ifdef TIMING_TEST
#include <time.h>
#define MAX_FRAMES 100

double ms;
static clock_t begin,end;
#endif

#ifdef TIMING_TEST2  
#define MAX_FRAMES 100
unsigned long ms;
unsigned long begin,end;
float delta;
#endif

// current mode info
SCREENINFO oldScreen;
SCREENINFO newScreenInfo;

void *pOldPhysbase,*pOldLogbase;
void *pPhysbase,*pLogbase;

// textures
sTexture *image,*texture,*logo;

sRect_t destRect;
int x_second,y_second;
int mode;
int blokOp,texOp;

#define ANGLE_STEP 0.45f
float angle=0.0f;

// main drawing function, draws to the logic/not visible screen
void draw(SCREENINFO *pScrnInfo);
int loadTextures(SCREENINFO *pInfo);
void unloadTextures();
int main(void);

// main program entry
int main(void){
    mode=0;
    logo=texture=image=0;
    blokOp=BLK_COPY;	//initial image copy block operation
    texOp=BLK_COPY;
    
#ifdef TIMING_TEST
    ms=0.0f;
#endif

    pOldPhysbase=pOldLogbase=pPhysbase=pLogbase=0;

    //clear screein information structures
    memset(&oldScreen,0,sizeof(SCREENINFO));
    memset(&newScreenInfo,0,sizeof(SCREENINFO));
    
    /* Install our asm handler */
    memset(Ikbd_keyboard, KEY_UNDEFINED, sizeof(Ikbd_keyboard));
    Ikbd_mousex = Ikbd_mousey = Ikbd_mouseb = Ikbd_joystick = 0;

    initDebug();

    //mandatory hardware check
    if(checkHardware()==0){
       logd("CT60 with pci bios not detected. Exiting.\n");
       deinitDebug();
       return 0;
    };
    
        if(initGEM()==-1){
            logd("GEM initialisation failed. Exiting.\n");
            deinitDebug();
            return 0;
       }
   
   long version = 0x0100;
   Vsetscreen(-1,&version,0x564E,CMD_GETVERSION);
   logd("Video XBIOS version %04lX\n", version);
   logd("Free Video RAM %ld\n", ct60_vmalloc(0,-1));
   
   logd("CTPCI check current mode\n") ;
   Vsetscreen(-1L,&mode,VN_MAGIC,CMD_GETMODE);

   logd("Current CTPCI video mode is: 0x%x\n",mode);

   // get old screen info:
   oldScreen.size=sizeof(SCREENINFO);
   oldScreen.devID=0;		//current mode
   oldScreen.scrFlags=0;	//reset status
   Vsetscreen(-1L,&oldScreen,VN_MAGIC,CMD_GETINFO);

   if(oldScreen.scrFlags & SCRINFO_OK){
       logd("Retrieved old screen mode.\n");
   }else{
       logd("Error: Couldn't retrieve info about current video mode.\n");
       deinitDebug();
       return(0);
   }

    //save old logbase and physbase
    pOldPhysbase=Physbase();
    pOldLogbase=Logbase();
    
   // ok we have done the init now we are allocating two screen buffers
   int iNewMode=VMODE;
 
   logd("Setting new graphics mode\n");
   //set new graphics mode
   Vsetscreen(-1L,iNewMode,VN_MAGIC,CMD_SETMODE);
   Vsync();
   
   pPhysbase=(void *)Physbase();

   long second=0;
   Vsetscreen(&second,iNewMode,VN_MAGIC,CMD_ALLOCPAGE);
   
   pLogbase=(void *)second;
    
   if(!second){
    logd("Couldn't allocate second frame buffer\n");
     deinitDebug();
     return(0);
    }
   
  logd("Setting new logbase\n"); //this doesn't change base address which is fixed
  Vsetscreen(pPhysbase,pLogbase,VN_MAGIC,CMD_SETADR);
  Vsync();
  
  logd("Current logbase: 0x%x and physbase: 0x%x\n",Logbase(),Physbase());
  
  //get new screen info
   newScreenInfo.size=sizeof(SCREENINFO);
   newScreenInfo.devID=0;			//current mode or iNewMode (it's the same)
   newScreenInfo.scrFlags=0;			//reset status
   Vsetscreen(-1L,&newScreenInfo,VN_MAGIC,CMD_GETINFO);

   if(newScreenInfo.scrFlags & SCRINFO_OK){
       logd("\nCurrently set video mode info:\n");
       printScreenInfo(&newScreenInfo);
   }else{
       logd("\nError: Couldn't retrieve info about current video mode.\n");
       deinitDebug();
       return(0);
   }
  
  long offset=second-(long)pPhysbase;
  int bpp=newScreenInfo.scrPlanes/8;

  x_second=(offset % (newScreenInfo.virtWidth*bpp))/bpp;
  y_second=offset / (newScreenInfo.virtWidth*bpp);

  logd("Second framebuffer at: x: %ld, y: %ld\n",x_second,y_second);
  
  destRect.Xpos=0;
  destRect.Ypos=y_second;	//second buffer
  destRect.width=640;
  destRect.height=480; 		//second logical screen
  destRect.block_op=BLK_SET;
  hwFillScreenRadeon(0x00ffffff,&destRect);
  
  //copy logical screen to physical buffer in hardware
  Vsetscreen(-1L,COPYPAGE_LOG_TO_PHYS,VN_MAGIC,CMD_COPYPAGE); 
  
  logd("Install IKBD handler.\n");
  Supexec(IkbdInstall);

    if(!loadTextures(&newScreenInfo)){
      systemRestore();
     return 0; 
    }
  
  int bQuit=1;
  
#ifdef TIMING_TEST
  unsigned long frames=0;
  begin=clock();
#endif

#ifdef TIMING_TEST2  
 //more accurate timing test based on 200hz system clock
  unsigned long frames=0;
  //go to supervisor, read system clock counter, return back to user mode
  long usp=Super(0);
  begin=*((long *)0x4ba);
  SuperToUser(usp);
#endif  
/******************************************** MAIN LOOP */
while (bQuit){
  
  for (int i=0; i<128; i++) {

      if (Ikbd_keyboard[i]==KEY_PRESSED){
          Ikbd_keyboard[i]=KEY_UNDEFINED;
	  
      switch(i){

        case SC_ESC:{
            bQuit=0;
        }break;

        case SC_HELP:{
	 //dumps all possible modes to file
         listRadeonModes();
	}break;
	case SC_T:{
	  texOp++;
	  if(texOp>BLK_SET) texOp=0;
	  logd("tex op: %s\n",getRadeonBlockOpName(texOp));
	}break;
	case SC_B:{
	  blokOp++;
	  if(blokOp>BLK_SET) blokOp=0;
	  logd("block op: %s\n",getRadeonBlockOpName(blokOp));
	}break;
        default:{;
	}
        }; // end of switch

        if (Ikbd_keyboard[i]==KEY_RELEASED) {
	  Ikbd_keyboard[i]=KEY_UNDEFINED;
	}
      } //if
    } //for
 
    draw(&newScreenInfo);
    
    //buffer swap
    Vsetscreen(-1L,-1L,VN_MAGIC,CMD_FLIPPAGE);
    Vsync();
    
    #ifdef TIMING_TEST
    //probably needs replacing with better one
    //because clock() isn't reliable on Atari's
    frames++;
   
    end=clock();
    ms=diffclock(end,begin); 

    if(frames==MAX_FRAMES||ms>=1000.0f){
      //calculate FPS
      double secs=ms/1000.0f;
      logd((const char*)"frames:%lu, avg. FPS:%6.4f, rendered in: ~%4.2f[sec](%6.4f [ms])\n",frames,frames/secs,secs,ms);
      frames=0;
      begin=clock(); 
    }
    #endif

    #ifdef TIMING_TEST2 
    //better version
    frames++;

    usp=Super(0);
    end=*((long *)0x4ba);
    SuperToUser(usp);

    delta=(end-begin)/200.0f;
    
    if(frames==MAX_FRAMES||delta>=1.0f){
      logd((const char*)"frames:%lu, avg. FPS:%3.2f, rendered in: ~%4.2f[sec]\n",frames,frames/delta,delta);
      frames=0;
      begin=end;
    }

    #endif
    
} //while
  
    systemRestore();
    return 0;
}
 
// draws stuff to the logic screen
void draw(SCREENINFO *pScrnInfo){
 static sRect_t src,dst;
 static sLine myLine;
 static long y_offset=0;
 static int line=0;
 static int goDown=0;
 static int goDown2=0;
 
 static int screenNb=0;
 
 int scrnHeight=pScrnInfo->virtHeight;
 int scrnWidth=pScrnInfo->virtWidth;
   
 //which buffer isn't displayed now?
 if(Physbase()==RADEON_BASE){
   screenNb=1;
  }else{
   screenNb=0;
 }

 y_offset=screenNb*y_second;
 
 //set screen to black
 dst.Xpos=0;
 dst.Ypos=y_offset;
 dst.width=scrnWidth;
 dst.height=scrnHeight; 	
 dst.block_op=BLK_SET;
 hwFillScreenRadeon(0x00000000L,&dst);

 //draw bg from image buffer
 src.Xpos=0;
 src.Ypos=image->y_offset;
 src.width=640;
 src.height=480;
 
 dst.Xpos=0;
 dst.Ypos=y_offset;
 dst.block_op=blokOp;
 
 hwCopyBlockRadeon(&src,&dst);
 
 /////////////////////// draw texture 
 //damn slow and inefficient. Loads texture from conventional memory (ST/TT-RAM)
 // to Radeon Video RAM and tiles it in dst memory block
 
//  src.Xpos=0;
//  src.Ypos=0;
//  src.width=256;
//  src.height=256;
//  
//  dst.Xpos=0;
//  dst.Ypos=y_offset; //actual offscreen buffer
//  dst.width=256;
//  dst.height=256;
//  dst.block_op=BLK_COPY;
//  
//  hwCopyTextureRadeon(texture->pImageData,&src,&dst);
//  
 
 //////////////////////////////////////////////////
 //put pixel
 dst.Xpos=scrnWidth/2.0f;
 dst.Ypos=scrnHeight/2.0f+y_offset;
 hwPutPixelRadeon(0x0000ff00L,&dst);
 
 //draw line
 if(line==(scrnHeight-1))goDown=1;
 if(line==0)goDown=0;
  
 if(goDown)line--;
    else
      line++;
 
 dst.Xpos=0;
 dst.Ypos=y_offset+line;
 dst.width=scrnWidth;
 dst.height=1; 	
 dst.block_op=BLK_COPY;
    
 //draw horizontal line, fast
 hwFillScreenRadeon(0x00ff0000,&dst);
 
// draws line, unoptimised Bresenham linerout.. 
// works slow because we set only one one pixel at once 
// instead of setting points in whole batches
 
//  myLine.x1=0;
//  myLine.y1=0;
//  myLine.x2=scrnWidth-1;
//  myLine.y2=scrnHeight-1;
//  myLine.color=0x0000FFFFL;
//  myLine.blockOp=BLK_SET;
//  hwDrawLineRadeon(&myLine,y_offset);

 //draw bg from image buffer
 src.Xpos=0;
 src.Ypos=logo->y_offset;
 src.width=640;
 src.height=120;
 
 if(angle>=45.0f)goDown2=1;
 if(angle<=0.0f)goDown2=0;
  
 if(goDown2)angle=angle-ANGLE_STEP;
    else
      angle=angle+ANGLE_STEP;
 
 float rad=angle*M_PI/180.0f;
 dst.Xpos=0;
 dst.Ypos=(int)((sin(rad)*(scrnHeight/2.0f))+y_offset+60);
 dst.block_op=texOp;
 hwCopyBlockRadeon(&src,&dst);
 
 // be happy :D + 8-# + \m/
}


//returns 1 if error
int loadTextures(SCREENINFO *pInfo){
  logd("Loading textures..\n");
  
  
  image = LoadTexture(BG_IMG,xRGB,MEMVRAM,pInfo);  

   if(!image){
     return 1;
   }
   
  texture = LoadTexture(TX_IMG,ARGB,MEMTTRAM,pInfo);  
   
   if(!texture){
     return 1;
   }
   
   logo = LoadTexture(LOGO_IMG,xRGB,MEMVRAM,pInfo);  
   
   if(!logo){
     return 1;
   } 
}

void unloadTextures(){
 logd("Releasing textures..\n");
 
  if(image){
      FreeTexture(image);
    }
   
   if(texture){
    FreeTexture(texture);
   }
   
   if(logo){
    FreeTexture(logo);
   }
}

void systemRestore(){
   //restore old videomode
   logd("Restoring video mode\n");
   
    //set screen(old logbase and physbase)
   Vsetscreen(pOldLogbase,pOldPhysbase,VN_MAGIC,CMD_SETADR);

   //set previous graphics mode
   Vsetscreen(-1L,mode,VN_MAGIC,CMD_SETMODE);
  
   //free logical screen
   Vsetscreen(-1,-1,VN_MAGIC,CMD_FREEPAGE);
   
   /* Uninstall our asm handler */
    Supexec(IkbdUninstall);
    deinitDebug();
    
    //free memory used for stored png image 
    unloadTextures();
    
    deinitGEM();
}

