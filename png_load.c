
#include "inc/png_load.h"
#include "inc/debug.h"
#include "inc/rad_help.h"

// loads texture into the memory 
sTexture *LoadTexture(char *pFilename, int targetFormat,int targetMem,SCREENINFO *pScrnInfo){
   sTexture *pTex=0;
   pTex=Mxalloc(sizeof(sTexture),MX_PREFTTRAM);
   int iSucess,bHasAlpha=0;
   
   if(pTex){
    strncpy(pTex->filename,pFilename,FILENAME_MAX*sizeof(char));
    logd("Loading texture image: %s\n",pFilename);

    iSucess=read_png(pTex->filename,&(pTex->width),&(pTex->height),&bHasAlpha, &(pTex->pImageData),targetMem);
    
    if(iSucess){
        logd("Image %s loaded sucessfully at %p...\nwidth: %d, height: %d alpha channel: %d\n",pTex->filename,pTex->pImageData,pTex->width,pTex->height,bHasAlpha);
	pTex->memflag=targetMem; //to indicate in which type of memory image is stored
	
	if(targetMem==MEMVRAM){
	  calculateVideoBufferOffset(pScrnInfo,pTex->pImageData,&(pTex->x_offset),&(pTex->y_offset));
	  
	}else{
	  pTex->x_offset=0;pTex->y_offset=0;
	}
	// loaded image is in RGBA format
	// and we need xRGB or ARGB, because this is Radeon target pixel format
	
	switch(targetFormat){
	 case xRGB:{
	   logd("Converting image to xRGB\n");
	   long *pPtr=(long *)pTex->pImageData;
	   for (int i=0;i<pTex->width*pTex->height;i++){
	      *pPtr=((*pPtr)>>8)&0x00ffffffL;pPtr++;
	   }
	 }break;
	 case ARGB:{
	   logd("Converting image to ARGB\n");
	   long *pPtr=(long *)pTex->pImageData;
	   
	   //convert RGBA->ARGB
	   long temp=0;
	   for (int i=0;i<pTex->width*pTex->height;i++){
	    temp=((*pPtr)&0x00000000FFL)<<16;
	    *pPtr=(((*pPtr)>>8)&0x00ffffffL)&temp;
	    pPtr++;
	  }
	 }
	
	 case RGBA:{
	    ;//do nothing
	  }
	 
	 default:{
	   ; //default
	 }break;
	};
    }
    else {
        logd("Cannot load image %s... Exiting..\n",pTex->filename);
	free(pTex);
        return 0;
    }
    return pTex; 
   }else 
     return 0;
  
}

// releases allocated memory 
void FreeTexture(sTexture *pTex){
   
if(pTex){
  switch(pTex->memflag){
    
    case MEMTTRAM:{ //TTRAM
      Mfree(pTex->pImageData);
      pTex->pImageData=0;
      Mfree(pTex);pTex=0;
    } break;
    case MEMVRAM:{ //VIDEORAM
      ct60_vmalloc(VRAM_FREE,pTex->pImageData);
      pTex->pImageData=0;
      Mfree(pTex);pTex=0;
    } break;
    
    default:{ //default TTRAM
      Mfree(pTex->pImageData);
      pTex->pImageData=0;
      Mfree(pTex);pTex=0;
    }break;
  }
}else 
  logd("FreeTexture() WARNING: texture pointer is null\n");
}

int read_png(char *file_name, unsigned int *iWidth, unsigned int *iHeight,unsigned int *bAlpha, unsigned char **outData,int targetMem)  
{
  // We need to open the file 
   png_structp png_ptr;
   png_infop info_ptr;
   unsigned int sig_read = 0;
   png_uint_32 width, height;
   int bit_depth, color_type, interlace_type;
   FILE *fp;

   if ((fp = fopen(file_name, "rb")) == NULL){
       logd("Couldn't open the file..\n"); return 0;
   }

   png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, 0, 0, 0);

   if (png_ptr == NULL){
      fclose(fp);
      logd("Create and initialize the png_struct failed...\n");
      return 0;
   }

   // Allocate/initialize the memory for image information.  REQUIRED. 
   info_ptr = png_create_info_struct(png_ptr);
   if (info_ptr == NULL){
      fclose(fp);
      png_destroy_read_struct(&png_ptr, png_infopp_NULL, png_infopp_NULL);
      logd("Error: Allocate/initialize the memory for image");
      return 0;
   }

   if (setjmp(png_jmpbuf(png_ptr))){
      // Free all of the memory associated with the png_ptr and info_ptr 
      png_destroy_read_struct(&png_ptr, &info_ptr, png_infopp_NULL);
      fclose(fp);
      // If we get here, we had a problem reading the file 
      logd("If we get here, we had a problem reading the file.\n");
      return 0;
   }

   png_init_io(png_ptr, fp);
   png_set_sig_bytes(png_ptr, sig_read);

   png_read_png(png_ptr, info_ptr, PNG_TRANSFORM_STRIP_16 | PNG_TRANSFORM_PACKING | PNG_TRANSFORM_EXPAND, png_voidp_NULL);

   /* At this point you have read the entire image */
   logd("PNG info w: %d\th:%d bits/channel:%d\tchannels: %d\t bytes per row: %d...\n", info_ptr->width, info_ptr->height,info_ptr->bit_depth,info_ptr->channels,info_ptr->rowbytes);

    *iWidth=info_ptr->width;
    *iHeight=info_ptr->height;

    switch (info_ptr->color_type) {
	        case PNG_COLOR_TYPE_RGBA:
	            logd("RGBA Alpha channel present.. [0x%x]\n",info_ptr->color_type);
	            //convert RGBA->ARGB, because it is supported in hardware
		    // and is easier to copy
		    //png_set_swap_alpha(png_ptr); 
		    *bAlpha=1;
	            break;
		
	        case PNG_COLOR_TYPE_RGB:
	            logd("RGB No Alpha channel present..[0x%x]\n",info_ptr->color_type);
	            *bAlpha=0;
            break;

	        default:{
	            logd("Color type ", info_ptr->color_type," not supported. Exiting...\n");
		    png_destroy_read_struct(&png_ptr, &info_ptr, png_infopp_NULL);
		    fclose(fp);
	            return 0;
	        }break;
	    };

        unsigned int row_bytes = info_ptr->rowbytes;

	*outData=NULL;
	if(targetMem==MEMVRAM){
	  logd("Allocating memory for output buffer in Video RAM.\n");

	  *outData =(unsigned char*)ct60_vmalloc(VRAM_ALLOC,(row_bytes *info_ptr->height*sizeof(int)));
	}else if (targetMem==MEMTTRAM){
	  logd("Allocating memory for output buffer in TT RAM.\n");
	  *outData=(unsigned char *)Mxalloc((row_bytes *info_ptr->height*sizeof(int)),MX_PREFTTRAM);
	}else{ 
	    //explicit default behavior
	    logd("Allocating memory for output buffer in default RAM.\n");
	    *outData=(unsigned char *)Mxalloc((row_bytes *info_ptr->height*sizeof(int)),MX_PREFTTRAM);
	}
	
        if(*outData==NULL){
           logd("Couldn't allocate memory for PNG data.\n");
           png_destroy_read_struct(&png_ptr, &info_ptr, png_infopp_NULL);
           fclose(fp);
	   return 0;
        }
        else{
            logd("Memory allocated sucessfully...\n");
        }

	    png_bytepp row_pointers=0;
	    row_pointers = png_get_rows(png_ptr, info_ptr);

	    for (int i = 0; i < info_ptr->height; i++) {
	        memcpy((*outData)+(row_bytes * i), row_pointers[i], row_bytes);
	    }

   // Clean up after the read, and free any memory allocated - REQUIRED 
   logd("read_png(): Clean up after the read, and free any memory allocated\n");
   png_destroy_read_struct(&png_ptr, &info_ptr, png_infopp_NULL);

   // Close the file 
   fclose(fp);
   logd("File loaded...\n");

   // That's it 
   return (1);
}
