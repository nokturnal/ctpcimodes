# ctpcimodes
Source code demonstrating setup of double buffering on Atari CT60 with CTPCI (PCI bridge) and ATI Radeon 7000 graphics card. Here is a video which I've recorded years ago:
https://vimeo.com/21062096

I'm putting this for historical / learning purposes (for example to show how to not design api's ;-)). Here is a rant/writeup about it(https://bus-error.nokturnal.pl/article14-ATI-Radeon-programming-with-XBIOS). In general use it at own risk.

(c) Pawel Goralski'2011