
//for GEM init/deinit
#include <gem.h> 
#include <assert.h>

// Atari - the only computer with built-in Cookie Jar :)
#include <mint/cookie.h>
#include "inc/rad_help.h"

//dumps SCREENINFO content to file/console
void printScreenInfo(SCREENINFO *pPtr){
 logd("\nSize of structure:\t0x%x\n",pPtr->size);
 logd("Current screen mode:\t0x%x\n",pPtr->devID);                          /* Device ID number           */
 logd("Name:\t%s\n",pPtr->name);                                           /* Friendly name of Screen    */
 logd("Flags:\t0x%x\n",pPtr->scrFlags);                                     /* Some flags                 */
 logd("Framebuffer address:\t0x%x\n",pPtr->frameadr,pPtr->frameadr);         /* Adress of framebuffer      */
 logd("Visible X res:\t0x%x (%d)\n",pPtr->scrWidth,pPtr->scrWidth);              /* Visible X res              */
 logd("Visible Y res:\t0x%x (%d)\n",pPtr->scrHeight,pPtr->scrHeight);               /* Visible Y res              */
 logd("Virtual X res:\t0x%x (%d)\n",pPtr->virtWidth,pPtr->virtWidth);             /* Virtual X res              */
 logd("Virtual Y res:\t0x%x (%d)\n",pPtr->virtHeight,pPtr->virtHeight);              /* Virtual Y res              */
 logd("color Planes:\t0x%x\n",pPtr->scrPlanes);                             /* color Planes               */
 logd("# of colors:\t0x%x (%d)\n",pPtr->scrColors,pPtr->scrColors);                /* # of colors                */
 logd("# of bytes to next line:\t0x%x\n",pPtr->lineWrap);     /* # of bytes to next line    */
 logd("# of bytes to next plane:\t0x%x\n",pPtr->planeWarp);   /* # of bytes to next plane   */
 logd("Screen format:\t0x%x\n",pPtr->scrFormat);              /* Screen format              */
 logd("CLUT type:\t0x%x\n",pPtr->scrClut);                    /* Type of clut               */
 logd("RED bits mask:\t0x%x\n",pPtr->redBits);                /* Mask of Red Bits           */
 logd("GREEN bits mask:\t0x%x\n",pPtr->greenBits);            /* Mask of Green Bits         */
 logd("BLUE bits mask:\t0x%x\n",pPtr->blueBits);              /* Mask of Blue Bits          */
 logd("ALPHA bits mask:\t0x%x\n",pPtr->alphaBits);            /* Mask of Alpha Bits         */
 logd("GENLOCK bits mask:\t0x%x\n",pPtr->genlockBits);        /* Mask of Genlock Bits       */
 logd("UNUSED bits mask:\t0x%x\n",pPtr->unusedBits);          /* Mask of unused Bits        */
 logd("Bits organisation flags:\t0x%x\n",pPtr->bitFlags);     /* Bits organisation flags    */
 logd("Max. memory in this mode:\t0x%x\n",pPtr->maxmem);      /* Max. memory in this mode   */
 logd("Memory size of one frame buffer page:\t0x%x (%d bytes)\n",pPtr->pagemem,pPtr->pagemem);     /* Needed memory for one page */
 logd("Max. screen width:\t0x%x\n",pPtr->max_x);              /* Max. possible width        */
 logd("Max. screen height:\t0x%x\n",pPtr->max_y);             /* Max. possible heigth       */
 logd("Refresh rate:\t%d [Hz]\n",pPtr->refresh);	      /* refresh rate in Hz */
 logd("Pixel clock:\t%d [pS]\n",pPtr->pixclock);   	      /* pixelclock */
 logd("=========================================\n");
}

//dumps SCRMEMBLK content to file/console
void printBlkInfo(SCRMEMBLK *pPtr){
 logd("\nSize of structure:\t0x%x\n",pPtr->size);          /* Size of structure */
 logd("Status bits of blk:\t0x%x\n",pPtr->blk_status);     /* Status bits of blk */
 logd("Start adress:\t0x%x\n",pPtr->blk_start);            /* Start adress */
 logd("Length of memblk:\t0x%x\n",pPtr->blk_len);          /* Length of memblk */
 logd("X pos in total screen:\t0x%x\n",pPtr->blk_x);       /* X pos in total screen */
 logd("Y pos in total screen:\t0x%x\n",pPtr->blk_y);       /* Y pos in total screen */
 logd("Width:\t0x%x\n",pPtr->blk_w);                       /* Width */
 logd("Height:\t0x%x\n",pPtr->blk_h);                      /* Height */
 logd("Width in bytes:\t0x%x\n",pPtr->blk_wrap);           /* Width in bytes */
 logd("=========================================\n");
}


void listRadeonModes(){
  logd("\nList video modes:\n");
  Vsetscreen(-1L,&videoEnumFunc,VN_MAGIC,CMD_ENUMMODES);
}

//calback for interrogating available video modes
//currently it dumps them all to the file (it may take *ALOT* of time)
int videoEnumFunc(SCREENINFO *inf, int flag){
    static unsigned int num=1;

    if(inf!=0)
	printScreenInfo(inf);
    else 
      logd("fatal error: SCREENINFO is NULL\n");

    num++;
    return ENUMMODE_CONT;
}


int checkHardware(void){
//if any ot these tests will fail, we are not dealing with ctpci equipped ct60
long mch=0,vdo=0,ct60=0,pci=0,modecode=0;

if(Getcookie(C__VDO, &vdo)==C_FOUND){
    if (vdo!=0x30000) {
      logd("No Atari Falcon VDO found. Exiting...\n");  
      return 0;
    }         //check VDO falcon
} else{ 
    logd("No VDO cookie found. Exiting...\n");  
  return 0;
}

if(Getcookie(C__MCH, &mch)==C_FOUND){
  if (mch!=0x30000){ 
       logd("This machine isn't Falcon 030. Exiting...\n");  
    return 0;           //check if falcon
  }
} else{ 
   logd("MCH cookie not found! Exiting...\n");  
  return 0; //nope
}
if(Getcookie(C_CT60, &ct60)!=C_FOUND){
  logd("CT60 not found! Exiting...\n");  
  return 0;
}                       //check ct60

if(Getcookie(C__PCI, &pci)!=C_FOUND){
  logd("CTPCI bus not found! Exiting...\n");  
 return 0;
}                        //check pci bios

//check graphics card availability
if((unsigned long)Physbase()<0x01000000UL){
  //graphics card not found, exit  
  // not very sufficient test, can give problems 
  // if another graphics card is connected  
  logd("Graphics card not found! Exiting...\n");  
  return 0;  
}

 Vsetscreen(-1,&modecode,VN_MAGIC,CMD_GETMODE);

 if(modecode!=(long)VsetMode(-1)) {
   logd("Wrong XBIOS version! Exiting...\n");  
   return 0;	//wrong XBIOS version
 }
 
 //if we are here, we got CTPCI, CT60, graphics card ...
 //you're a worthy user ;)
 return 1;
}

void calculateVideoBufferOffset(SCREENINFO *scrnInfo,void *p,int *x_offset,int *y_offset){

  //maybe use get info instead passing scrnInfo?

  assert(p>RADEON_BASE); //function works only for Radeon Video RAM
 //if assertion failed, pointer doesn't point to the Radeon memory
 
 long offset=(long)p-(long)RADEON_BASE;
 int bpp=scrnInfo->scrPlanes/8;

 *y_offset= offset / (scrnInfo->virtWidth*bpp);
 *x_offset= (offset % (scrnInfo->virtWidth*bpp))/bpp;
 
 logd("Texture VRAM offset x:%d, y:%d\n",*x_offset,*y_offset);
 
}

GRECT r,rect;
  
int initGEM(){
  short int gr_hwchar,gr_hhchar,gr_hwbox,gr_hhbox;
	
	if(appl_init()<0){
	  logd("GEM initialisation failed. appl_init()<0 \n");
	  return(-1);
	}
	
	graf_handle(&gr_hwchar,&gr_hhchar,&gr_hwbox,&gr_hhbox);
	wind_update(BEG_UPDATE);
	wind_update(BEG_MCTRL);
	graf_mouse(M_OFF,(MFORM *)0);
	rect.g_x=rect.g_y=0;                /* screen size */
	wind_get(0,WF_WORKXYWH,&r.g_x,&r.g_y,&r.g_w,&r.g_h); /* desktop */
	rect.g_w=r.g_x+r.g_w-1;
	rect.g_h=r.g_y+r.g_h-1;
	return 0;
}

void deinitGEM(){
	graf_mouse(M_ON,(MFORM *)0);
	graf_mouse(ARROW,(MFORM *)0);
	wind_update(END_MCTRL);
	wind_update(END_UPDATE);
	appl_exit();
}

static const char *g_arRadeonBlockOpNames[]={
  "BLK_CLEAR",
  "BLK_AND",          
  "BLK_ANDREVERSE",   
  "BLK_COPY",         
  "BLK_ANDINVERTED",  
  "BLK_NOOP",         
  "BLK_XOR",          
  "BLK_OR",           
  "BLK_XNOR",         
  "BLK_EQUIV",        
  "BLK_INVERT",       
  "BLK_ORREVERSE",    
  "BLK_COPYINVERTED", 
  "BLK_ORINVERTED",   
  "BLK_NAND",         
  "BLK_SET"          
};

const char *getRadeonBlockOpName(int id){
  if(id<BLK_CLEAR||id>BLK_SET)return 0;
  return g_arRadeonBlockOpNames[id];
}
