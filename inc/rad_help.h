#ifndef _RAD_HELP_H_
#define _RAD_HELP_H_

/**
    Copyright 2010-2011 Pawel Goralski
    e-mail: pawel.goralski@nokturnal.pl
    See license.txt for licensing information.
*/

#include "ctpci_defs.h"

//gem init/deinit
int initGEM();
void deinitGEM();

// displays SCREENINFO structure
void printScreenInfo(SCREENINFO *pPtr);

//dumps all available video modes to file
void listRadeonModes();

//list all available video modes
static int videoEnumFunc(SCREENINFO *inf, int flag);

//function check if running this program on current hardware is sane
//returns 1 if there if current machine is CT60 equipped Falcon 
// with CTPCI and active graphics card
//it returns 0 otherwise

int checkHardware(void);

//calback for interrogating available video modes (CMD_ENUMMODES)
//currently it dumps them all to the file (it may take *ALOT* of time)

int videoEnumFunc(SCREENINFO *inf, int flag);

//calculates coordinates where texture is stored in VRAM memory, which are used by
//hardware accelerated Radeon functions
void calculateVideoBufferOffset(SCREENINFO *scrnInfo,void *p,int *x_offset,int *y_offset);

//gets Radeon block operation name
const char *getRadeonBlockOpName(int id);

#endif