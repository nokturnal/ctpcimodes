#ifndef __CTPCI_DEFS_H__
#define __CTPCI_DEFS_H__

/**
    Copyright 2010-2011 Pawel Goralski
    e-mail: pawel.goralski@nokturnal.pl
    See license.txt for licensing information.
*/

#include <mint/sysbind.h>
#include <mint/osbind.h>
#include <mint/ostruct.h>

//this is for older mintlibs
//Super() bug fix
#ifndef SuperToUser
#define SuperToUser (ptr) Super(ptr)
#endif

#include "debug.h"

// Vsetscreen modecode extended flags 
#define HORFLAG         0x200 /* double width */
#define HORFLAG2        0x400 /* width increased */
#define VESA_600        0x800 /* SVGA 600 lines */
#define VESA_768       0x1000 /* SVGA 768 lines */
#define VERTFLAG2      0x2000 /* double height */
#define DEVID          0x4000 /* bits 11-3 used for devID */
#define VIRTUAL_SCREEN 0x8000 /* width * 2 and height * 2, 2048 x 2048 max */
#define BPS32 5

// OVERSCAN and PAL flags are used for select refresh frequency:
// OVERSCAN | PAL | Freq
// ---------+-----+-----
//        0 |  0  | 56
//        0 |  1  | 60
//        1 |  0  | 70
//        1 |  1  | 85 
//

#define GET_DEVID(x) (((x) & DEVID) ? ((x & 0x3FF8) >> 3) : -1)
#define SET_DEVID(x) ((((x) << 3) & 0x3FF8) | DEVID)

#define VN_MAGIC 0x564E       //CTPCI video mode specific value

// Vsetscreen function enumeration
enum{
    CMD_GETMODE=0,
    CMD_SETMODE,
    CMD_GETINFO,
    CMD_ALLOCPAGE,
    CMD_FREEPAGE,
    CMD_FLIPPAGE,
    CMD_ALLOCMEM,
    CMD_FREEMEM,
    CMD_SETADR,
    CMD_ENUMMODES,
    CMD_TESTMODE,
    CMD_COPYPAGE,
    CMD_FILLMEM,
    CMD_COPYMEM,
    CMD_TEXTUREMEM,
    CMD_GETVERSION
};

#define BLK_ERR      0
#define BLK_OK       1
#define BLK_CLEARED  2

//defines for CMD_COPYPAGE 
#define COPYPAGE_LOG_TO_PHYS 1
#define COPYPAGE_PHYS_TO_LOG 0

typedef struct _scrblk
{
  long size;                  /* size of structure           */
  long blk_status;            /* status bits of blk          */
  long blk_start;             /* Start Address               */
  long blk_len;               /* length of memblk            */
  long blk_x;                 /* x pos in total screen       */
  long blk_y;                 /* y pos in total screen       */
  long blk_w;                 /* width                       */
  long blk_h;                 /* height                      */
  long blk_wrap;              /* width in bytes              */
} __attribute__((packed)) SCRMEMBLK;

/* block operations */
enum{
  BLK_CLEAR=0,
  BLK_AND,          
  BLK_ANDREVERSE,   
  BLK_COPY,         
  BLK_ANDINVERTED,  
  BLK_NOOP,         
  BLK_XOR,          
  BLK_OR,           
  BLK_XNOR,         
  BLK_EQUIV,        
  BLK_INVERT,       
  BLK_ORREVERSE,    
  BLK_COPYINVERTED, 
  BLK_ORINVERTED,   
  BLK_NAND,         
  BLK_SET          
};

typedef struct{
  long Xpos,Ypos;	//block start position
  long width,height;	//width and height of the block
  long block_op;	//block operation type
}__attribute__((packed)) sRect_t,*pSRect_t;

typedef struct _scrfillblk
{
  long size;                  /* size of structure           */
  long blk_status;            /* status bits of blk          */
  long blk_op;                /* mode operation              */
  long blk_color;             /* background fill color       */
  long blk_x;                 /* x pos in total screen       */
  long blk_y;                 /* y pos in total screen       */
  long blk_w;                 /* width                       */
  long blk_h;                 /* height                      */
  long blk_unused;
} __attribute__((packed)) SCRFILLMEMBLK;
             
typedef struct _scrcopyblk
{
  long size;                 /* size of structure            */
  long blk_status;           /* status bits of blk           */
  long blk_src_x;            /* x pos source in total screen */
  long blk_src_y;            /* y pos source in total screen */
  long blk_dst_x;            /* x pos dest in total screen   */
  long blk_dst_y;            /* y pos dest in total screen   */
  long blk_w;                /* width                        */
  long blk_h;                /* height                       */
  long blk_op;		     /* block operation */
}__attribute__((packed)) SCRCOPYMEMBLK;

typedef struct _scrtextureblk
{
  long size;                /* size of structure             */
  long blk_status;          /* status bits of blk            */
  long blk_src_x;           /* x pos source                  */
  long blk_src_y;           /* y pos source                  */
  long blk_dst_x;           /* x pos dest in total screen    */
  long blk_dst_y;           /* y pos dest in total screen    */
  long blk_w;               /* width                         */
  long blk_h;               /* height                        */
  long blk_op;              /* mode operation                */
  long blk_src_tex;         /* source texture address        */
  long blk_w_tex;           /* width texture                 */
  long blk_h_tex;           /* height texture                */
}__attribute__((packed)) SCRTEXTUREMEMBLK;

/* scrFlags */
#define SCRINFO_OK 1

/* scrClut */
#define NO_CLUT    0
#define HARD_CLUT  1
#define SOFT_CLUT  2

/* scrFormat */
#define INTERLEAVE_PLANES  0
#define STANDARD_PLANES    1
#define PACKEDPIX_PLANES   2

/* bitFlags */
#define STANDARD_BITS  1
#define FALCON_BITS    2
#define INTEL_BITS     8

#define ENUMMODE_CONT 1
#define ENUMMODE_EXIT 0

typedef struct screeninfo
{
  long size;        /* Size of structure          */
  long devID;       /* modecode                   */
  char name[64];    /* Friendly name of Screen    */
  long scrFlags;    /* some Flags                 */
  long frameadr;    /* Address of framebuffer     */
  long scrHeight;   /* visible X res              */
  long scrWidth;    /* visible Y res              */
  long virtHeight;  /* virtual X res              */
  long virtWidth;   /* virtual Y res              */
  long scrPlanes;   /* color Planes               */
  long scrColors;   /* # of colors                */
  long lineWrap;    /* # of Bytes to next line    */
  long planeWarp;   /* # of Bytes to next plane   */
  long scrFormat;   /* screen Format              */
  long scrClut;     /* type of clut               */
  long redBits;     /* Mask of Red Bits           */
  long greenBits;   /* Mask of Green Bits         */
  long blueBits;    /* Mask of Blue Bits          */
  long alphaBits;   /* Mask of Alpha Bits         */
  long genlockBits; /* Mask of Genlock Bits       */
  long unusedBits;  /* Mask of unused Bits        */
  long bitFlags;    /* Bits organisation flags    */
  long maxmem;      /* max. memory in this mode   */
  long pagemem;     /* needed memory for one page */
  long max_x;       /* max. possible width        */
  long max_y;       /* max. possible heigth       */
  long refresh;	    /* refresh rate in Hz */
  long pixclock;    /* pixelclock */
} __attribute__((packed)) SCREENINFO;

#define BLK_ERR      0
#define BLK_OK       1
#define BLK_CLEARED  2

#define RADEON_BASE 0x40000000UL

// video memory allocation routine 
#define VRAM_FREE 1
#define VRAM_ALLOC 0

#define ct60_vmalloc(mode,value) (long)trap_14_wwl((short int)0xc60e,(short int)(mode),(long)(value))


#endif
