#ifndef _DEBUG_H_
#define _DEBUG_H_

/**
    Copyright 2010-2011 Pawel Goralski
    e-mail: pawel.goralski@nokturnal.pl
    See license.txt for licensing information.
*/
#include <time.h>

//debugging OUTPUT settings
#define DEBUG_FILE_OUTPUT 1     //enable file output
#define OUTPUT_BUFFER_SIZE 256  //output buffer size

void initDebug();
void deinitDebug();
void logd(const char *mes,...);

// utility for measuring function time execution
static inline double diffclock(clock_t end,clock_t begin){
 double diffticks=end-begin;
 double diffms=(diffticks)/(CLOCKS_PER_SEC/1000.0f);
 return diffms;
}

#endif