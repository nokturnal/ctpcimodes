/**
    Copyright 2010-2011 Pawel Goralski
    e-mail: pawel.goralski@nokturnal.pl
    See license.txt for licensing information.
*/

#include "ctpci_defs.h"
#include "assert.h"

typedef struct spixel{
  int x;
  int y;
  unsigned long color;
  unsigned long blockOp;
}__attribute__((packed)) sPixel;

typedef struct sline{
  int x1,y1;
  int x2,y2;
  unsigned long color;
  unsigned long blockOp;
}__attribute__((packed)) sLine;

//function copies block of video memory to the other fragment of video memory with given blit operation
static inline void hwCopyBlockRadeon(sRect_t *srcRect,sRect_t *dstRect){
 static SCRCOPYMEMBLK blk;
 
 blk.size=sizeof(SCRCOPYMEMBLK);
 blk.blk_status=0;
 blk.blk_src_x=srcRect->Xpos;
 blk.blk_src_y=srcRect->Ypos;
 blk.blk_w=srcRect->width;
 blk.blk_h=srcRect->height;
 
 //set destination
 blk.blk_dst_x=dstRect->Xpos;
 blk.blk_dst_y=dstRect->Ypos;
 blk.blk_op=dstRect->block_op;
 
 Vsetscreen(-1,&blk,VN_MAGIC,CMD_COPYMEM); 
 
 if (blk.blk_status!=BLK_OK) logd("Error: CMD_COPYMEM failed!\n");
  
}

//function copies texture block to video buffer with given blit operation
static inline void hwCopyTextureRadeon(void *srcTexBuf,sRect_t *srcRect,sRect_t *dstRect)
{
 static SCRTEXTUREMEMBLK src;
 src.size=sizeof(SCRTEXTUREMEMBLK); //boring stuff
 src.blk_status=0;
 
 //more intersting stuff
 src.blk_src_tex=srcTexBuf;
 
 //set source
 src.blk_src_x=srcRect->Xpos;
 src.blk_src_y=srcRect->Ypos;
 src.blk_w_tex=srcRect->width;
 src.blk_h_tex=srcRect->height;
 
 //set destination
 src.blk_dst_x=dstRect->Xpos;
 src.blk_dst_y=dstRect->Ypos;
 src.blk_w=dstRect->width;
 src.blk_h=dstRect->height;
 src.blk_op=dstRect->block_op;
 
 //go!
 Vsetscreen(-1,&src,VN_MAGIC,CMD_TEXTUREMEM);
 if(src.blk_status!=BLK_OK) logd("Error during copyblock(CMD_TEXTUREMEM) operation\n");
}

//function fills video buffer with given fillColor value 
// and with given block operation type
static inline void hwFillScreenRadeon(long fillColor,sRect_t *destRect){
  static SCRFILLMEMBLK fill;
  
  fill.size = sizeof(SCRFILLMEMBLK);
  fill.blk_status = 0;
  fill.blk_op = destRect->block_op;     /* mode operation */
  fill.blk_color = fillColor; 		/* background fill color */
  fill.blk_x = destRect->Xpos; 		/* x pos in total screen */
  fill.blk_y = destRect->Ypos; 		/* y pos in total screen */
  fill.blk_w = destRect->width; 	/* width  */
  fill.blk_h = destRect->height; 	/* height */
  
  Vsetscreen(-1,&fill,VN_MAGIC,CMD_FILLMEM);
}

//puts pixels at given coordinates with given block operation and color
static inline void hwPutPixelRadeon(long fillColor,sRect_t *destRect){
  static SCRFILLMEMBLK put;
  
  put.size = sizeof(SCRFILLMEMBLK);
  put.blk_status = 0;
  put.blk_op = destRect->block_op;     /* mode operation */
  put.blk_color = fillColor; 		/* background fill color */
  put.blk_x = destRect->Xpos; 		/* x pos in total screen */
  put.blk_y = destRect->Ypos; 		/* y pos in total screen */
  put.blk_w = 1; 	/* width  */
  put.blk_h = 1; 	/* height */
  
  Vsetscreen(-1,&put,VN_MAGIC,CMD_FILLMEM);
}

#include <stdlib.h>
static inline void hwDrawLineRadeon(sLine *pLine, int y_offset){
  static SCRFILLMEMBLK put;

  static float dx,dy,p,end;
  static float x,y;
  
  put.size = sizeof(SCRFILLMEMBLK);
  put.blk_status = 0;

  dx=abs(pLine->x1-pLine->x2);
  dy=abs(pLine->y1-pLine->y2);
  
  if(pLine->x1 > pLine->x2){
            x = pLine->x2;
            y = pLine->y2;
            end = pLine->x1;
   }
   else{
     x = pLine->x1;
     y = pLine->y1;
     end = pLine->x2;
   }
  
  put.blk_op = pLine->blockOp;     /* mode operation */
  put.blk_color = pLine->color;        /* background fill color */
  put.blk_x = x; 		       /* x pos in total screen */
  put.blk_y = y + y_offset; 	       /* y pos in total screen */
  put.blk_w = 1; 		       /* width  */
  put.blk_h = 1; 	               /* height */
  
  Vsetscreen(-1,&put,VN_MAGIC,CMD_FILLMEM);
   
      while(x < end){
       x = x + 1;
       
       if(p < 0){
              p = p + 2 * dy;
       }else{ y = y + 1;
              p = p + 2 * (dy - dx);
       }
       
       put.blk_op = pLine->blockOp;     	/* mode operation */
       put.blk_color = pLine->color; 		/* background fill color */
       put.blk_x = x; 				/* x pos in total screen */
       put.blk_y = y+y_offset; 			/* y pos in total screen */
       put.blk_w = 1; 				/* width  */
       put.blk_h = 1; 				/* height */
       Vsetscreen(-1,&put,VN_MAGIC,CMD_FILLMEM);
      }
}
