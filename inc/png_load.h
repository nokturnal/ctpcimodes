#ifndef _PNG_LOAD_H_
#define _PNG_LOAD_H_

/**
    Copyright 2010-2011 Pawel Goralski
    e-mail: pawel.goralski@nokturnal.pl
    See license.txt for licensing information.
*/

#include "ctpci_defs.h"

// libpng includes
#include <png.h>
#include <stdlib.h>

#ifndef png_jmpbuf
#  define png_jmpbuf(png_ptr) ((png_ptr)->jmpbuf)
#endif

#define MEMTTRAM 0
#define MEMVRAM 1

enum{
  xRGB=0,
  RGBA,
  ARGB
};

typedef struct stexture {
  int size;	   //size of the structure
  char filename[FILENAME_MAX];
  int width,height;
  int format;	     //pixel format xRGB,RGBA,ARGB etc.
  int memflag; 	     //indicates if the image is loaded into Radeon VRAM or in TT Ram
  int x_offset;	     //precalculated x offset in radeon memory (to avoid calculating it each frame) not applicable for conventional RAM
  int y_offset;	     //precalculated y offset in radeon memory (to avoid calculating it each frame) not applicable for conventional RAM
  void *pImageData;  //image data itself
} __attribute__((packed)) sTexture;

// loads texture into the memory (TT RAM or Video RAM),
// pScrnInfo only needed when loading texture into VRAM
sTexture *LoadTexture(char *pFilename,int targetFormat,int targetMem,SCREENINFO *pScrnInfo);

// releases allocated memory 
void FreeTexture(sTexture *pTex);

//loads png from file
int read_png(char *file_name, unsigned int *iWidth, unsigned int *iHeight,unsigned int *bAlpha, unsigned char **outData,int targetMem);

#endif