
#    Copyright 2010-2011 Pawel Goralski
#    See license.txt for licensing information.

prefix = /usr/m68k-atari-mint

INCLUDES = -I./ -I./include -I$(prefix)/include
CC = m68k-atari-mint-gcc
GAS = m68k-atari-mint-as
STRIP = m68k-atari-mint-strip
MACHINE = -m68020-60
MACHINE_AS = -march=68060
PNG_CFLAGS = `$(prefix)/bin/libpng12-config --cflags`
PNG_LIBS = `$(prefix)/bin/libpng12-config --ldflags` -lm -lz
SHARED_FOLDER = ~/Pulpit/shared

# add -DTIMING_TEST to CFLAGS for simple FPS logging
# add -DTIMING_TEST2 to CFLAGS for FPS logging based on 200hz system timer

CFLAGS += --verbose -std=c99 $(MACHINE) $(INCLUDES) -Wall -fsigned-char -fomit-frame-pointer -pedantic $(PNG_FLAGS)
LDFLAGS += $(MACHINE) -L/usr/m68k-atari-mint/lib -Wl,--traditional-format -lm -lgem $(PNG_LIBS)

EXE = test.tos

SRC = test.c debug.c png_load.c rad_help.c
OBJ = test.o debug.o png_load.o rad_help.o

$(EXE): $(OBJ) ikbd_asm.o  
		$(CC) $(OBJ) ikbd.o -o $@ $(LDFLAGS) 
		cp ./$(EXE) $(SHARED_FOLDER) 

ikbd_asm.o:	ikbd.S
		$(GAS) $(MACHINE_AS) ikbd.S -o ikbd.o

$(OBJ): %.o: %.c
	$(CC) -c $(CFLAGS) $< -o $@


all: 	$(SRC) $(EXE)
clean:
	rm -rf *o $(EXE)

.PHONY: default clean

