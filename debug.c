
#include "inc/debug.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>

#ifdef DEBUG_FILE_OUTPUT
static FILE *ofp;
#endif

#ifndef DEBUG_LOG
#define DEBUG_LOG "LOG.TXT"
#endif


void logd(const char *mes,...){
    static char buffer[256];

    va_list va;
    va_start(va,mes);
    //snprintf(buffer,((OUTPUT_BUFFER_SIZE*sizeof(char))-1),(const char *)mes,va);
    vsprintf(buffer,(const char *)mes,va);
    va_end(va);

#ifdef CON_LOG
    fprintf(stdout,buffer);
#endif

#ifdef DEBUG_FILE_OUTPUT
    fprintf(ofp,buffer);
    fflush(ofp);
#endif
 return;
}

void initDebug(){
#ifdef DEBUG_FILE_OUTPUT
    ofp=NULL;
    ofp=fopen(DEBUG_LOG,"w");

    if(ofp==NULL){
        fprintf(stderr,"Can't init file output: %s\n",DEBUG_LOG);
    }
#endif
 return;
}

void deinitDebug(){
#ifdef DEBUG_FILE_OUTPUT
    fclose(ofp);
#endif
 return;
}
